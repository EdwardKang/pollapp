class Response < ActiveRecord::Base
  attr_accessible :answer_choice_id, :user_id
  validates :answer_choice_id, presence: true
  validates :user_id, presence: true
  validate :respondent_has_not_already_answered_question
  validate :author_cant_respond_to_own_polls

  belongs_to(
    :answer_choice,
    class_name: 'AnswerChoice',
    foreign_key: :answer_choice_id,
    primary_key: :id
  )

  belongs_to(
    :respondent,
    class_name: 'User',
    foreign_key: :user_id,
    primary_key: :id
  )

  def respondent_has_not_already_answered_question
    params = [self.user_id, self.answer_choice.question_id]
    query = <<-SQL, params[0], params[1]
    SELECT responses.user_id AS id
    FROM responses
    JOIN answer_choices
    ON responses.answer_choice_id = answer_choices.id
    WHERE responses.user_id = ? AND answer_choices.question_id = ?
    SQL

    results = Response.find_by_sql(query)
    if results.length > 0 && results.first.id != self.id
      errors[:add_response] << "Can't respond to the same question twice!"
    end
  end

  def author_cant_respond_to_own_polls
    #ac_id = User.select("answer_choices.*").joins(authored_polls: [{questions: :answer_choices}])

    #p ac_id
    #ac_id
    User.find(self.user_id).questions.each do |question|
      if question.answer_choices.include?(AnswerChoice.find(self.answer_choice_id))
         errors[:add_response] << "Can't respond to own polls"
      end
    end

    #To find the poll author, I used AR to do a series of joins, from User through :polls through :questions through :answer_choices. Look up how to do series of joins if you don't know how.

#I then selected the row with the appropriate answer_choices.id.

  end


end

