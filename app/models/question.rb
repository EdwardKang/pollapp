class Question < ActiveRecord::Base
  attr_accessible :poll_id, :question_text
  validates :poll_id, presence: true
  validates :question_text, presence: true

  belongs_to(
    :poll,
    class_name: 'Poll',
    foreign_key: :poll_id,
    primary_key: :id
  )

  has_many(
    :answer_choices,
    class_name: 'AnswerChoice',
    foreign_key: :question_id,
    primary_key: :id
  )

     #posts = self.posts.includes(:comments)

  def results
    answer_choices = self.answer_choices.includes(:responses,:question)

    answer_choice_counts = {}
    answer_choices.each do |answer_choice|
      answer_choice_counts[answer_choice.answer_text] = answer_choice.responses.length
    end

    answer_choice_counts
  end

  def sql_results
    #subquery: get all answer_choices for a particular question.
    #BIG_query: get count of responses group by answer_choice_id
    query =
    "SELECT
      COUNT(responses.id), answer_choices.answer_text
    FROM
      answer_choices
    LEFT OUTER JOIN
      responses ON answer_choices.id = responses.answer_choice_id
    WHERE
      question_id = #{self.id}

    GROUP BY
      answer_choice_id"

    results = ActiveRecord::Base.connection.execute(query)
    res_hash = {}
    results.each do |count_hash|
      res_hash[count_hash["answer_text"]] = count_hash["COUNT(responses.id)"]
    end

    res_hash
  end
  #[{"COUNT(responses.id)"=>0, 0=>0}, {"COUNT(responses.id)"=>1, 0=>1}]


end
