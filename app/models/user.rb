class User < ActiveRecord::Base
  attr_accessible :user_name
  validates :user_name, presence: true, uniqueness: true

  has_many(
    :authored_polls,
    class_name: 'Poll',
    foreign_key: :author_id,
    primary_key: :id
  )

  has_many(
    :responses,
    class_name: 'Response',
    foreign_key: :user_id,
    primary_key: :id
  )

  has_many :questions, through: :authored_polls, source: :questions
 #Write the methods completed_polls (polls where the user has answered all of the questions in the poll) and uncompleted_polls (polls that the user has started but not completed).

  def completed_polls
    #look at responses, then get all the polls and questions
    responses = self.responses

  end

  def incomplete_polls
  end
end
