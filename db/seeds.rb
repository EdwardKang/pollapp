# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

ActiveRecord::Base.transaction do
  u1 = User.create!(
    user_name: "Ed"
  )

  u2 = User.create!(
    user_name: 'Fred'
  )

  vote = Poll.create!(
    author_id: 1,
    title: 'Football q?'
  )

  q1 = Question.create!(
    poll_id: 1,
    question_text: 'What NFC team do you like best?'
  )

  q2 = Question.create!(
    poll_id: 1,
    question_text: 'Best QB in the NFC?'
  )


  a11 = AnswerChoice.create!(
    question_id: 1,
    answer_text: 'NY Giants'
  )

  a12 = AnswerChoice.create!(
    question_id: 1,
    answer_text: 'Cowboys'
  )

  a21 = AnswerChoice.create!(
    question_id: 2,
    answer_text: 'Eli Manning'
  )

  a22 = AnswerChoice.create!(
    question_id: 2,
    answer_text: 'Tony Romo'
  )

  r = Response.create!(
    user_id: 1,
    answer_choice_id: 2
  )





end